int led=9; //PIN PWM LED

void setup() {
  // put your setup code here, to run once:
  pinMode(led,OUTPUT);
}

void loop() {
  int i=0;

  for(i=0;i<256;i++){
    analogWrite(led,i);
    delay(10);
  }
  delay(1000);
  for(i=0;i<256;i++){
    analogWrite(led,255-i);
    delay(10);
  }
  // put your main code here, to run repeatedly:

}
