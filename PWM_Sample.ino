const int PIN=9;//PIN DESDE DONDE SALDRÁ LA SEÑAL PWM, IMPORTANTE QUE ESTÉ MARCADO CON LA '~', CONSULTAR LA INFORMACIÓN DE LA PLACA EN CASO DE QUE NO VENGA MARCADA
int PWM=0;
void setup() {
  // put your setup code here, to run once:
  pinMode(PIN,OUTPUT);//CONFIGURACIÓN DEL PIN DIGITAL 9 COMO SALIDA
  
}

void loop() {
  // put your main code here, to run repeatedly:
  PWM=map(250,0,500,0,255);//SE CONFIGURA PARA QUE EL PWM DE UN VOLTAJE EFICAZ DE 2.50 V USANDO LA FUNCIÓN MAP
  analogWrite(PIN,PWM);

  /*
  La función PWM se utiliza para simular una salida analógica, que en la mayoría de placas de Arduino es imposible puesto que no cuentan con DAC.
  La idea es que dependiendo del intervalo de actividad e inactividad de la señal, se genere un voltaje eficaz más cercano a los 5.00V (si hay más tiempo a 5V) o más cercano a 0V

  En el caso del arduino UNO, la resolución del PWM es de 8 bits, es decir, analogWrite requiere un valor entre 0 y 255, siendo 255 una señal continua de 5V y 0 una señal continua de 0V
  */
}
