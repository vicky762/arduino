const int pot=A2;//VARIABLE EN LA QUE SE ALMACENA EL PIN DEL POTENCIÓMETRO
const int led=6;//EL DEL LED

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);//SE INICIA LA COMUNICACIÓN PUERTO SERIE A UNA VELOCIDAD DE 9600
  pinMode(led,OUTPUT);//SE CONFIGURA EL led COMO SALIDA
}

void loop() {
  // put your main code here, to run repeatedly:
  int lectura=analogRead(pot);//SE HACE LA LECTURA ANALÓGICA Y SE ALMACENA EN pot
  int volt=0;
  volt=map(lectura,0,1023,0,5000);SE INTERPOLA LA LECTURA PARA SACAR MILIVOLTIOS QUE HAY EN LA ENTRADA ANALÓGICA
  Serial.println(volt);
  if(volt>=2500){
    digitalWrite(led,HIGH);
  }
  else{
    digitalWrite(led,LOW);
  }
}
