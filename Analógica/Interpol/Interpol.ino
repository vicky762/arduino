const int pin=A1; //PIN DONDE CONECTAMOS LA SALIDA DE NUESTRO SENSOR (RECORDAR QUE HAY QUE CONECTARLO EN SERIE CON UNA RESISTENCIA DE 10K, Y CONECTAR ESTE PIN EN MEDIO DE LA RESISTENCIA Y EL SENSOR

float intepol(float v){//FUNCIÓN QUE NOS DEVOLVERÁ LA TEMPERATURA SEGÚN LA TENSIÓN QUE ENTRE EN EL PIN ANALÓGICO
  float temp=20.00;
  temp+=-17.93*(v-2.77); 
  temp-=0.62*(v-2.77)*(v-2.71);
  temp-=4.99*(v-2.77)*(v-2.71)*(v-2.66);
  temp-=6.01*(v-2.77)*(v-2.71)*(v-2.66)*(v-2.60);
  temp-=-4802.97*(v-2.77)*(v-2.71)*(v-2.66)*(v-2.60)*(v-2.65);
  
  /*
  Se puede hacer el cálculo directamente o por pasos, que es lo que he hecho yo. 
  Para cálculos j**idos os recomiendo hacerlo por pasos para no perderos y para
  que haya menos probabilidades de error. Eso si, esto es un consejo, el código que
  hagáis es vuestro y os lo podéis f*** como más os guste :)
  */
  return temp;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); //Iniciación del puerto serie (Recuerdo que hay modelos que tienen más de un puerto serie, si hay dudas, consultar los datos de vuestro modelo de placa.
}



void loop() {
  // put your main code here, to run repeatedly:
  int lanalog=analogRead(pin);//Se hace la lectura analógica
  float volt=lanalog*5.00/1023.00; //Se convierte en voltaje
  float temperatura=intepol(volt);//Se hace la interpolación
  Serial.println(lanalog);
  Serial.println(volt);
  Serial.println(temperatura);//Se muestran el valor de la entrada analógica, la tensión de entrada, y la temperatura del sensor
  delay(1000);
}
