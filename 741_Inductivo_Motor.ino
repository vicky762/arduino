
const int ind=2;//PIN ASIGNADO A LA SALIDA DEL AMPLIFICADOR
const int rel=3;//PIN QUE CONTROLA EL RELÉ DEL MOTOR



void setup() {
  
  pinMode(ind,INPUT);//Configura el ind como entrada
  pinMode(rel,OUTPUT);//Configura el rel como salida
  digitalWrite(rel,LOW);//Pone un 0 lógico en rel

}

void loop() {
  if(digitalRead(ind)==HIGH){//Si detecta un objeto metálico
    digitalWrite(rel,HIGH);
    delay(3000);
    digitalWrite(rel,LOW);
    delay(2000);
    /*
     * El programa enciende el motor durante 3 segundos, y luego permanece a la espera durante 2.
     * Es recomendable NO USAR DELAYs, ya que detienen el arduino por completo
     */
  }
  else{
    digitalWrite(rel,LOW);//Si no se detectan objetos metálicos, el relé se desconecta
  }
 

}

/*enlaces a los esquemas de conexión
 * 
 * Circuito del amplificador operacional: https://media.discordapp.net/attachments/767438129371807835/767438144827817994/unknown.png?width=754&height=658
 * Circuito del motor: https://media.discordapp.net/attachments/767438129371807835/767438340503896104/unknown.png?width=1033&height=658
 * 
 */
 //IMPORTANTE: TODOS LOS PINES DE GND DEBEN IR CONECTADOS PARA TENER LA MISMA REFERENCIA
