#include <Servo.h>

//CON LIBRERIA
Servo myservo;

const int pot=2;//PIN DEL POTENCIOMETRO
const int servo=6;//PIN DEL SERVOMOTOR 

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);//Inciamos el puerto serie para ver el ángulo del motor
  pinMode(servo,OUTPUT);//Definimos el pin del servo como salida
  myservo.attach(servo);
}

void loop() {
  int valor=0,angulo=0;//Variables de |Lectura analógica del potenciómetro|Angulo teórico|
  valor=analogRead(pot);//LEE EL POTENCIOMETRO
  angulo=map(valor,0,1023,0,180);//CONVIERTE EL VALOR DEL POTENCIÓMETRO EN UN ÁNGULO ENTRE 0 Y 180
  Serial.print("Angulo: ");//SE MUESTRA EN EL PUERTO SERIE EL ÁNGULO CALCULADO
  Serial.println(angulo);
  myservo.write(angulo);
}
