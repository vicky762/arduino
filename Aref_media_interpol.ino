//EN ESTE CÓDIGO ESTARÍAN RESUMIDOS LOS 3 TUTORIALES DE ELECTRÓNICA ANALÓGICA

int val;//VARIABLES GLOBALES

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
  /*
   * Función para iniciar la comunicación Arduino-PC
   * 9600 baudios es la velocidad del puerto serie y suele ser la más común
  */
}

int media(int n/*EL NUMERO DE MEDICIONES*/,int pin/*EL NUMERO DE PIN DONDE VAMOS A MEDIR*/){
  int i;//VARIABLES LOCALES
  long unsigned int sum=0;
  int resultado;
  for(i=0;i<n;i++){
    analogReference(EXTERNAL);
    sum=sum+analogRead(pin);
    delay(20);
  }
  resultado=sum/n;
  return resultado;
  //Esta función corresponde al segundo vídeo
}

int interpol(int x){//Función de polinomio interpolador, la usamos para un cálculo más preciso que la función map. La idea convertir esa lecutra en un voltaje, que estará entre 0 y 3.3 V
  int p;
  p=3.31*x-0.000134*x*(x-305)+0.0000000357*x*(x-305)*(x-596);
  return p;
  //Esta función corresponde al tercer vídeo
}

void loop() {
  int vmedio;//Variable local, solo existe en la función loop
  analogReference(EXTERNAL);//Establece el voltaje máximo de entrada analógica por el que le llega al pin AREF. Este voltaje debe estar entre 1V y 5V
  vmedio=media(50,3);//Se calcula la media y se vuelca el resultado en la variable vmedio
  val=map(vmedio,0,1023,0,3300);//La medida convertida en voltaje por la función map
  Serial.print("Valor funcion map: ");
  Serial.print(val);
  Serial.println(" mV");
  val=interpol(vmedio);//La medida convertida en voltaje por medio de una interpolación polinómica
  Serial.print("Valor interpol: ");
  Serial.print(val);
  Serial.println(" mV");
  delay(1000);
  // put your main code here, to run repeatedly:

}
