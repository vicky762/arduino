const int led=5;//Pin conectado al led
const long fast=9600;//Velocidad del puerto serie
void setup(){
   Serial.begin(fast);//inciación del puerto serie a la velocidad especificada en 'fast'
   pinMode(led,OUTPUT); 
}

void loop(){
  if(Serial.available() > 0 ){//Espera a que entre información por el puerto serie
      char data = Serial.read();//Almacena el primer dato que lee en una variable tipo char.
      if(data == '1'){
        digitalWrite(led,HIGH);
      }
      else if(data== '0'){
        digitalWrite(led,LOW); 
      }
      Serial.println(data);//Muestra el valor de 'data' en pantalla
  }
}
