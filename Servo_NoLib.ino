//CODIGO SIN LIBRERIA

const int pot=2;//PIN DEL POTENCIOMETRO
const int servo=6;//PIN DEL SERVOMOTOR 


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);//Inciamos el puerto serie para ver el ángulo del motor
  pinMode(servo,OUTPUT);//Definimos el pin del servo como salida
}

void loop() {
  int valor=0,angulo=0,tiempo=0;//Variables de |Lectura analógica del potenciómetro|Angulo teórico|Conversión a tiempo
  valor=analogRead(pot);//LEE EL POTENCIOMETRO
  angulo=map(valor,0,1023,0,180);//CONVIERTE EL VALOR DEL POTENCIÓMETRO EN UN ÁNGULO ENTRE 0 Y 180
  tiempo=map(angulo,0,180,1000,2000);//PARA PODER MOVER EL SERVO, SE CALCULA EL TIEMPO DE ACTIVIDAD DEL MOTOR EN FUNCIÓN DEL ÁNGULO SIENDO 0º 1000 us y 180 2000 us
  Serial.print("Angulo: ");//SE MUESTRA EN EL PUERTO SERIE EL ÁNGULO CALCULADO
  Serial.println(angulo);
  digitalWrite(servo,HIGH);
  delayMicroseconds(tiempo);//SE MANDA UNA SEÑAL DE 5V AL SERVO QUE DURARÁ tiempo (en microsegundos);
  digitalWrite(servo,LOW);//SE MANDA UNA SEÑAL DE 0V AL SERVO QUE DURARÁ LO QUE RESTE DE TIEMPO HASTA 20 milisegundos
  delay(20-tiempo/1000);

}
